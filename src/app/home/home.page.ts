import { Component, OnInit } from '@angular/core';
import { RallyQuestion } from '../../rallyquestion';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  //Member variables
  questions: RallyQuestion[] = [];
  activeQuestion: RallyQuestion;
  isCorrect: boolean;
  feedback: string;
  questionCounter: number;
  optionCounter: number;

  startTime: Date;
  endTime: Date;
  duration: number;

  constructor(public router: Router) {}

  ngOnInit() {
    this.questions = [{
      quote: 'Kuka heistä ajaa M-Sport WRC -autolla vuonna 2019?',
      options: [
        'Sebastien Ogier',
        'Elfyn Evans',
        'Esapekka Lappi'],
        correctOption: 1
      },
      {
        quote: 'Kuka voitti rallin SM -sarjan SM1 luokassa vuonna 2019?',
        options: [
          'Teemu Asunmaa',
          'Emil Lindholm',
          'Juha Salo'],
          correctOption: 0
      },
      {
        quote: 'Mikä ralli käynnistää rallin mm-sarjan vuonna 2020?',
        options: [
          'Ruotsi',
          'Meksiko',
          'Monte-Carlo'],
          correctOption: 2
      },
      {
        quote: 'Mistä voit nähdä MM-sarjan jokaisen erikoiskokeen livenä?',
        options: [
          'Yle',
          'Red Bull TV',
          'WRC All live'],
          correctOption: 2
      }
  ];


    this.startTime = new Date();

    this.questionCounter = 0;
    this.setQuestion();

  }

  setQuestion() {
    if (this.questionCounter === this.questions.length) {
      this.endTime = new Date();
      this.duration = this.endTime.getTime() - this.startTime.getTime();
      this.router.navigateByUrl('results/' + this.duration);
      this.ngOnInit();
    } else {
    this.optionCounter = 0;
    this.feedback = '';
    this.isCorrect = false;
    this.activeQuestion = this.questions[this.questionCounter];
    this.questionCounter++;
  }
}
  checkOption(option: number, activeQuestion: RallyQuestion) {
    this.optionCounter++;
    if (this.optionCounter > activeQuestion.options.length) {
      this.setQuestion();
    }
    if (option === activeQuestion.correctOption) {
      this.isCorrect = true;
      this.feedback = activeQuestion.options[option] +
      ' Oikein! Paina ralliautoa kerran!';
    } else {
      this.isCorrect = false;
      this.feedback = 'Väärin. Number of quesses left: ' +
      (this.activeQuestion.options.length - this.optionCounter);
    }
  }
}
