export class RallyQuestion {
    quote: string;
    options: string[];
    correctOption: number;
}
